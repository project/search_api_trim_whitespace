<?php

namespace Drupal\search_api_trim_whitespace\Plugin\search_api\processor;

use Drupal\search_api\Processor\FieldsProcessorPluginBase;

/**
 * Plugin to remove errant whitespace and non-printable characters from text.
 *
 * @SearchApiProcessor(
 *   id = "trim_whitespace",
 *   label = @Translation("Trim Whitespace"),
 *   description = @Translation("Tries to remove any erroneous whitespace, multiple spaces, and spaces before punctuation in text before it is stored. Best used after the HTML Filter on text fields."),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = 0,
 *     "preprocess_query" = 0,
 *   }
 * )
 */
class TrimWhitespace extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function processFieldValue(&$value, $type) {
    if (!$this->getDataTypeHelper()->isTextType($type, ['text', 'string'])) {
      return $value;
    }

    $value = str_replace("&nbsp;", '', $value);

    // Remove multiple spaces.
    $value = preg_replace('/( {2,})+/imu', ' ', $value);

    // Remove spaces before punctuation.
    $value = preg_replace('/\s+([!?.,])/imu', "$1", $value);

    // Remove any space at the start of a string.
    $value = preg_replace('/^\s+/imu', '', $value);

    // Remove any non-printable characters.
    $value = preg_replace('/[[:^print:]]/imu', '', $value);

    $value = trim($value);
  }

}
