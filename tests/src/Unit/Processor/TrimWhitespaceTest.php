<?php

namespace Drupal\Tests\search_api_trim_whitespace\Unit\Processor;

use Drupal\search_api_trim_whitespace\Plugin\search_api\processor\TrimWhitespace;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\search_api\Unit\Processor\ProcessorTestTrait;
use Drupal\Tests\search_api\Unit\Processor\TestItemsTrait;

/**
 * Tests the "Trim Whitespace" processor.
 *
 * @group search_api_trim_whitespace
 *
 * @see \Drupal\search_api_trim_whitespace\Plugin\search_api\processor\TrimWhitespace
 */
class TrimWhitespaceTest extends UnitTestCase {

  use ProcessorTestTrait;
  use TestItemsTrait;

  /**
   * Creates a new processor object for use in the tests.
   */
  public function setUp(): void {
    parent::setUp();
    $this->setUpMockContainer();
    $this->processor = new TrimWhitespace([], 'trim_whitespace', []);
  }

  /**
   * Tests preprocessing field values with whitespace issues.
   *
   * @param string $passed_value
   *   The value that should be passed into process().
   * @param string $expected_value
   *   The expected processed value.
   *
   * @dataProvider stringsContainingWhitespaceProvider
   */
  public function testTrimWhitespaceRemovesWhitespace($passed_value, $expected_value, string $data_type = 'text') {
    $this->invokeMethod('processFieldValue', [&$passed_value, $data_type]);
    $this->assertEquals($expected_value, $passed_value);
  }

  /**
   * Data provider for testTrimWhitespaceRemovesWhitespace().
   *
   * @return array
   *   An array of test values and expected results
   *   for testTrimWhitespaceRemovesWhitespace().
   */
  public function stringsContainingWhitespaceProvider() {
    return [
      [
        '  word',
        'word',
      ],
      [
        '&nbsp;word',
        'word',
      ],
      [
        'word      ',
        'word',
      ],
      [
        '  word            ',
        'word',
      ],
      [
        '  This is a  sentence . Therefore , please correct it   .',
        'This is a sentence. Therefore, please correct it.',
      ],
      [
        'word .',
        'word.',
      ],
      [
        'word    .',
        'word.',
      ],
      [
        'The dash in this sentence - it should be left alone. But , the comma should be fixed.',
        'The dash in this sentence - it should be left alone. But, the comma should be fixed.',
      ],
      [
        'If I were to    "Quote someone" , the quote marks should be okay !',
        'If I were to "Quote someone", the quote marks should be okay!',
      ],
      [
        '&nbsp;&nbsp;SomeText&nbsp;&nbsp;&nbsp;MoreText&nbsp;&nbsp;',
        'SomeTextMoreText',
      ],
      [
        '&nbsp;&nbsp;SomeText &nbsp;&nbsp;&nbsp;MoreText&nbsp;&nbsp;',
        'SomeText MoreText',
      ],
      [
        ' The start of a sentence should not have a space.',
        'The start of a sentence should not have a space.',
      ],
      [
        '&nbsp;&nbsp;SomeText &nbsp;&nbsp;&nbsp;MoreText&nbsp;&nbsp;',
        'SomeText MoreText',
        'string',
      ],
      [
        '  This is a  sentence . Therefore , please correct it   .',
        'This is a sentence. Therefore, please correct it.',
        'string',
      ],
    ];
  }

}
