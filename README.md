CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

This module adds a processor for Search API that attempts to
remove errant whitespace and non-printable characters from
stored text.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/search_api_trim_whitespace

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/search_api_trim_whitespace


REQUIREMENTS
------------

This module requires Search API.


INSTALLATION
------------

* Install the module as you would normally install a
  contributed Drupal module.


CONFIGURATION
-------------

    1. Navigate to your search index in the Drupal admin.
    2. Click on the "Processors" tab.
    3. Enable the "Trim Whitespace" processor.
    4. Configure the text fields to run the processor on.
    5. Save the settings.
    6. Re-index your content.

Be sure to place this processor after the HTML Filter
on the index for the best possible results.

MAINTAINERS
-----------

Current maintainers:
* Kevin Quillen (kevinquillen) - https://kevinquillen.com

Supporting organizations:
* Velir - https://www.drupal.org/velir
